




config = {

# The name of the module
    'module_name': "",

# The input keys and their types
    'module_inputs': [],

# The output keys and their types
    'module_outputs': [],

# If the script can be run multiple times in parallel to allow for concurrent execution.
# Don't do this if your information are sequential and you need the previous ouput for the next input.
    'multiprocessing_allowed': True,

# If the script should be run locally, even if a remote connection is available.
# This is important if you depend on specific files or hardware, that you want to use. (Webcam of your own laptop, file of your current pc etc.)
    'force_local': False,

# The length of the history that should be kept for the inputs.
    'history_length': 1,

# If the module requires a webcam
    'webcam_required': False,

# If the module requires a GPU
    'gpu_required': False,

# This setting should only be set to true, if the module is intended to send data without any input. (Think a module sending a video file with every frame)
# Otherwise this setting can be ignored. IF this is set, the module expects no module inputs and WILL ignore the 'module_inputs' setting.
# For an example of a sender_override, please see the example module "WebcamSender" and "VideoSender"
#    'sender_override': True,

# The interval in which the sender_override should be called. (in executions per second (Hz).
# This setting will take an integer, if the module should be executed multiple times per second or a float value with value <1 if the module should be executed every few seconds.
#    'sender_override_interval':30,

# --------------------------------------------------------------
# If you want to display the config in the GUI, please use one of the classes from the parameter_configs.py
# For example:
    #   'processing_steps':NumberConfig(name="Processing steps", min=1, max=10, default=5, description="The number of processing steps to be done.")
    #   'model_name':StringConfig(name="Model Name", default="YOLOV8n",choices=["YOLOV8n","YOLOV8s","YOLOV8m","YOLOV8l","YOLOV8x"], description="The model to use for pose estimation."),
    #   'video_file':FileChooserConfig(name="Videofile path", default="", description="The path to the video file that should be used.")
# This would include a number input field with the name "Processing steps" in the GUI with the range from 1 to 10 and a default value of 5.
# --------------------------------------------------------------

}