import asyncio
import os

import torch

import module_config
from enrico_compute.configs.parameter_configs import NumberConfig, FileChooserConfig
from enrico_compute.module_base import BaseCompute

os.environ["ENRICO_LIVE_RUN"] = "true"

class bert_impl(BaseCompute):

    def __init__(self):

        config = module_config.config
        config["module_name"] = "bert_processor"
        config["module_inputs"] = {"text_input":str}
        config["module_outputs"] = {"text_output":str}
        config["multiprocessing_allowed"] = True
        config["force_local"] = False
        config["history_length"] = 5
        config["gpu_required"] = True
        config["webcam_required"] = False
        config["frame_rate"] = NumberConfig(name="Frame Rate", min=1, max=60, default=30, description="The frame rate of the webcam stream.")
        #config['video_file'] = FileChooserConfig(name="Videofile path", default="", description="The path to the video file that should be used.")

        self.config = config

        super().__init__(config)


    def load(self):
        """
        load all necessary files and models for the module.
        Please do not run this method manually or load your data in the __init__ function,
        as the framework uses this function to calculate the RAM and VRAM requirements.
        """

        #load all necessary models
        from transformers import pipeline
        self.unmasker = pipeline('fill-mask', model='bert-base-uncased',device="cuda" if torch.cuda.is_available() else "cpu")

    def example_input(self):
        """
        Please define an example input for the module, with the same keys as the module inputs.
        This way we can calculate RAM and VRAM usages and warmup the model for the pipeline

        @return: dict with example inputs with the same keys as in config.module_inputs
        """
        return {"text_input":"Hello I'm a [MASK] model."}

    def compute(self, data_input: dict):
        """
        The compute function that is executed every time new data is transmitted.

        @param data_input: A dictionary with the same keys as config.module_inputs, with the last transmitted data. Additionally we provide the last transmitted data up to length: config.history_length
        Please beware, as we handle network connections and only part of the inputs could be available, there could be None values in the data input if the module just started and not all connections have transmitted some data.
        PLEASE CHECK FOR THIS!

        @return: a new dictionary with the result of your calculations. This dictionary should contain the same keys as in config.module_outputs.
        If you only have a partial output, return only the newly calculated keys. If an error is occured, please return None.
        """

        ret = self.unmasker(data_input["text_input"])
        return {"text_output":ret,}



if __name__ == "__main__":
    bert = bert_impl()
    bert.start()
