# -*- coding: utf-8 -*-
from setuptools import setup, find_packages


NAME = "enrico_compute"
AUTHOR = 'Hendric Voß'
DESCRIPTION = 'The base compute model, that every other module should import.'
URL = 'https://gitlab.ub.uni-bielefeld.de/scs/enrico/modules/base_compute_module'
EMAIL = "hvoss@techfak.uni-bielefeld.de"
VERSION = "0.1"

REQUIRED = ["numpy", "psutil","asyncio","imagezmq","pyzmq","ipaacar-python"]


setup(
    name=NAME,
    version=VERSION,
    description=DESCRIPTION,
    author=AUTHOR,
    author_email=EMAIL,
    url=URL,
    packages=find_packages(),
    py_modules=["enrico_compute"],
    zip_safe=False,
    install_requires=REQUIRED,
    license='MIT'
)
