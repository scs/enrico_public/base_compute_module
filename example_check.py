import asyncio
import logging
import sys

import numpy
import numpy as np

from enrico_compute.connection.connection_zmq import SenderConnectionZMQ
import asyncio

from enrico_compute.connection.connection_ipaaca_information import MessageObj, MessageReason, ConnectionRequest, Connection_Ipaaca
from enrico_compute.connection.connection_zmq import SenderConnectionZMQ



def dummy(msg):
    pass
async def ipaaca():
    conn_info = Connection_Ipaaca(dummy, communication_host="radioactiveman", communication_port="1883", communication_identifier="test_run")
    await conn_info.create_communication_connection()
    msg = MessageObj()
    msg.uuid_from = ""
    msg.uuid_to = "1"
    msg.msg_reason = MessageReason.CONNECT_REQUEST
    msg.connection_request = ConnectionRequest()
    msg.connection_request.Host = "127.0.0.1"  # the connection host
    msg.connection_request.Port = "43529"  # the connection port
    msg.connection_request.ConnectionName = "image"  # our outbounding connection that should be used to send to the other connection.
    msg.connection_request.Uuid = "-1"

    await conn_info.sendMessage(msg)

async def main():
    port = "50114"
    sender = SenderConnectionZMQ("127.0.0.1", port, str)
    await sender.send_data(1)

async def numpy_send():
    port = "43948"
    sender = SenderConnectionZMQ("127.0.0.1", port, np.ndarray)
    await sender.send_data(np.zeros((640,480,3)))

if __name__ == "__main__":
    log = logging.getLogger('')
    log.setLevel(logging.DEBUG)
    ch = logging.StreamHandler(sys.stdout)
    log.addHandler(ch)
    asyncio.run(numpy_send())

