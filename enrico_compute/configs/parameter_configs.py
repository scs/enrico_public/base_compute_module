from abc import abstractmethod


class ConfigBase:

    def get_min(self):
        return 0

    def get_max(self):
        return 0

    def getChoices(self):
        return 0

    @abstractmethod
    def get_default(self):
        return 0

    def get_description(self):
        return ""

    def get_name(self):
        return ""

    def get_value(self):
        return self.get_default()

    def set_value(self,data):
        pass


class NumberConfig(ConfigBase):

    def __init__(self,name,min,max,default,description=""):
        self.min = min
        self.max = max
        self.default = default
        self.name = name
        self.description = description
        self.value = default

    def get_min(self):
        return self.min

    def get_max(self):
        return self.max

    def get_default(self):
        return self.default

    def get_name(self):
        return self.name

    def get_description(self):
        return self.description

    def get_value(self):
        return self.value

    def set_value(self,data):
        self.value = data


class StringConfig(ConfigBase):

    def __init__(self,name,default,choices=None,description=""):
        self.default = default
        self.choices = choices
        self.name = name
        self.description = description
        self.value = default

    def get_default(self):
        return self.default

    def getChoices(self):
        return self.choices

    def get_name(self):
        return self.name

    def get_description(self):
        return self.description

    def get_value(self):
        return self.value

    def set_value(self,data):
        self.value = data


class FileChooserConfig(ConfigBase):

    def __init__(self, name, default, description=""):
        self.default = default
        self.name = name
        self.description = description
        self.value = default

    def get_default(self):
        return self.default

    def get_name(self):
        return self.name

    def get_description(self):
        return self.description

    def get_value(self):
        return self.value

    def set_value(self,data):
        self.value = data