import asyncio
import logging

from enrico_compute.connection.connection_ipaaca_information import MessageReason, MessageStatus, MessageObj


class ConnLogHandler(logging.StreamHandler):

    def __init__(self,uuid):
        logging.StreamHandler.__init__(self)
        fmt = '%(asctime)s - %(filename)s - %(levelname)s:  %(message)s'
        fmt_date = '%Y-%m-%dT%T%Z'
        formatter = logging.Formatter(fmt, fmt_date)
        self.setFormatter(formatter)
        self.uuid = uuid
        self.conn = None
        self.msg_hist = []

    def setConnection(self,conn):
        self.conn = conn

    async def async_send(self):
        while True:
            if self.conn is not None:
                todel = []
                for msg in self.msg_hist:
                    await self.conn.sendMessage(msg)
                    todel.append(msg)

                for msg in todel:
                    self.msg_hist.remove(msg)

                await asyncio.sleep(0.5)
            else:
                await asyncio.sleep(1)

    def thread_sendMessages(self):
        loop = asyncio.new_event_loop()
        loop.run_until_complete(loop.create_task(self.async_send()))


    def emit(self, record):

        #format and stdout
        msg_format = self.format(record)
        print(msg_format)

        # send log to broker
        if record.levelname in ("INFO","DEBUG"):
            reason = MessageReason.LOG_INFO
            status = MessageStatus.OK
        else:
            reason = MessageReason.ERROR_INFO
            status = MessageStatus.ERROR

        msg = MessageObj()
        msg.uuid_from = self.uuid
        msg.msg_reason = reason
        msg.status = status
        msg.msg_body = msg_format
        self.msg_hist.append(msg)
