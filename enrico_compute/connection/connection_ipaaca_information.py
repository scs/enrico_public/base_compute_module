import asyncio
import codecs
import copy
import logging
import pickle
import traceback
from dataclasses import dataclass
from enum import IntEnum

from ipaacar.components import create_mqtt_pair
from ipaacar.handler import MessageCallbackHandlerInterface

class ServiceMessageObj:
    uuid_from = None  # uuid : str
    uuid_to = None  # uuid : str

    hostname = None  # name : str
    ip = None  # ip : str
    msg_reason = None  # MessageReason : Enum
    msg_ack_reason = None  # MessageReason : Enum

    msg_body = "" # str

    available_module_list = None # moduleinfo : dict

    container_name = None # container_name : str # for container start/stop
    container_uuid = None # container_id : str # for container start/stop



class MessageStatus(IntEnum):
    STARTUP = 0
    OK = 1
    ERROR = 2
    SHUTDOWN = 3

class MessageReason(IntEnum):
    SERVICE_STARTUP_INFO = 0
    SERVICE_SHUTDOWN = 1

    PING_INFO = 2
    PING_REQUEST = 3
    LOG_INFO = 4
    ERROR_INFO = 5

    CONTAINER_STARTUP_INFO = 6
    CONTAINER_SHUTDOWN_INFO = 7

    CONTAINER_START_REQUEST = 8
    CONTAINER_STOP_REQUEST = 9

    CONNECT_REQUEST = 10
    DISCONNECT_REQUEST = 11

    SENDER_DONE = 12

    SERVICE_SHUTDOWN_REQUEST = 13

    REGISTRY_RELOAD_REQUEST = 14

    ACKNOWLEDGE = 15

    CONFIG_DATA_REQUEST = 16

    VISUALIZE_REQUEST = 17



class ReceiverConnectionsInfo():

    def __init__(self):
        self.__open_receiver_connections = {} # tuple of (host,port,datatype,connection_name)
        self.local_network_ip = None

    def addOpenConnection(self,port,datatype,connection_name,host=None):
        #check if we want to automatically determine internal network ip address
        if host is None:
            #lookup is expensive, so we cache the result. TODO: Maybe invalidate after some time?
            host = self.local_network_ip if self.local_network_ip is not None else get_ip_address()
            self.local_network_ip = host

        self.__open_receiver_connections[connection_name] = (host,port,datatype,connection_name)
        return (host,port,datatype,connection_name)

    def removeOpenConnection(self,connection_name):
        #delete if exists
        self.__open_receiver_connections.pop(connection_name,None)

    def getConnections(self):
        #copy probably unnecessary
        return copy.copy(list(self.__open_receiver_connections.values()))

@dataclass
class ConnectionRequest:

    ConnectionName: str = None
    Uuid: str = None

    Host: str = None
    Port: str = None

class LoggerInfo:

    Info_msg = None
    Exception_msg = None


class MessageObj:

    uuid_from = None #uuid : str
    uuid_to = None #uuid : str

    modulename = None #name : str
    status = None #MessageStatus : Enum
    msg_reason = None # MessageReason : Enum
    msg_ack_reason = None # MessageReason : Enum

    msg_body = None # MessageBody : str

    open_receiver_connections = None # ReceiverConnectionInfo : class
    connection_request = None # ConnectionRequest : class
    error_info = None #LoggerInfo : class

    ram_usage = None # ram usage in bytes
    vram_usage = None # vram usage in bytes

    module_config = None # dict of module config values



import socket
def get_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.settimeout(0)
    try:
        # doesn't even have to be reachable
        s.connect(('10.254.254.254', 1))
        IP = s.getsockname()[0]
    except Exception:
        traceback.print_exc()
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP


class MessageHandler(MessageCallbackHandlerInterface):
    """We inherit from a different ABC"""
    def __init__(self,callback_function):
        self.callback_function = callback_function

    async def process_message_callback(self, msg: str):
        """We receive a pickled message obj as str and unpickle it"""
        #logging.debug("Got Message")
        unpickled = pickle.loads(codecs.decode(msg.encode(), "base64"))
        self.callback_function(unpickled)

class Connection_Ipaaca():


    def __init__(self,callback_func,communication_host="radioactiveman",communication_port="1883",communication_identifier=""):
        self.host = communication_host
        self.port = communication_port
        self.callback_func = callback_func
        self.communication_identifier = communication_identifier


    async def create_communication_connection(self):
        self.ib, self.ob = await create_mqtt_pair("ib_msg", "ob_msg", f"module_{self.communication_identifier}", f"{self.host}:{self.port}")
        await self.ib.listen_to_category(f"enrico_{self.communication_identifier}")
        await self.ib.on_new_message(MessageHandler(self.callback_func))

    async def add_Listener(self,listener):
        await self.ib.listen_to_category(f"enrico_{listener}")

    async def sendMessage(self,msgobj):
        pickled = codecs.encode(pickle.dumps(msgobj), "base64").decode()
        await self.ob.send_message(f"enrico_{self.communication_identifier}",pickled)