import json
import logging
import os.path
import sys
import threading
import time
import uuid
from sys import flags

import asyncio
from typing import Dict, Any

import numpy
import zmq
import zmq.asyncio
from zmq.utils.monitor import recv_monitor_message

from enrico_compute.connection.connection_base import connection_base


logger = logging.getLogger(__name__)

def getuuid():
    return str(uuid.uuid4().hex)

class FileTransfer:
    pass

class ReceiverConnectionZMQ(connection_base):

    def __init__(self, host, port, datatype, conn_name,callback_function=None):
        super().__init__()
        self.host = host
        self.port = port
        self.datatype = datatype
        self.conn_name = conn_name
        self.do_connection()
        self.callback_function = callback_function
        #self.connection_num = 0
        #t = threading.Thread(target=self.event_monitor,daemon=True)
        #t.start()


    def event_monitor(self):

        async def run():
            conn_event_socket = self.conn.get_monitor_socket(events=zmq.EVENT_HANDSHAKE_SUCCEEDED)
            while True:
                event = await recv_monitor_message(conn_event_socket)
                print(event)

        loop = asyncio.new_event_loop()
        loop.run_until_complete(run())

    def do_connection(self):
        if 'conn' in locals():
            self.conn.close()
        self.ctx = zmq.asyncio.Context()
        self.conn = self.ctx.socket(zmq.REP)
        #self.conn.setsockopt(zmq.RCVTIMEO,30*1000) # 30s timeout
        self.conn.bind(f"tcp://{self.host}:{self.port}")
        logger.debug(f"binding to tcp://{self.host}:{self.port}")


        #self.poll = zmq.asyncio.Poller()
        #self.poll.register(self.conn, zmq.POLLIN)

        if isinstance(self.datatype,(numpy.ndarray, numpy.generic)) or self.datatype == numpy.ndarray:
            logger.debug(f"Setting up numpy data receive for {self.conn_name}")
            self.receive_data = self.receive_numpy_data
        elif isinstance(self.datatype,(str,int,float,list,dict,bool)) or any(self.datatype == c for c in (str,int,float,list,dict,bool)):
            logger.debug(f"Setting up native data receive for {self.conn_name}")
            self.receive_data = self.receive_native_data
        elif isinstance(self.datatype,FileTransfer) or self.datatype == FileTransfer:
            logger.debug(f"Setting up file receive for {self.conn_name}")
            self.receive_data = self.receive_file
        else:
            logger.warning(f"Unknown datatype: {self.datatype}")

    #def run(self):
    #
        # loop = asyncio.new_event_loop()
        # while True:
        #     logging.info(f"Waiting for data on {self.conn_name}")
        #     loop.run_until_complete(self.receive_data())



    async def receive_native_data(self,autorequeue=False):
        data = await self.conn.recv_string()
        data = json.loads(data)
        logger.debug(f"received data: {data}")
        await self.conn.send_string(json.dumps({"time":time.time()}))
        logger.debug(f"send reply: {time.time()}")

        ret = {"connection_name":self.conn_name,"data":data}
        if autorequeue:
            asyncio.ensure_future(asyncio.create_task(self.receive_data(autorequeue=autorequeue)))
        if self.callback_function:
            self.callback_function(ret)
        else:
            return ret

    async def receive_numpy_data(self,autorequeue=False):
        md = await self.conn.recv_string()
        logger.debug("received numpy string")
        msg = await self.conn.recv()
        logger.debug("received numpy data")

        md = json.loads(md)
        logger.debug(f"received data: {md}")
        data = numpy.frombuffer(msg, dtype=md['dtype'])
        data = data.reshape(md['shape'])

        await self.conn.send_string(json.dumps({"time":time.time()}))
        logger.debug(f"send reply: {time.time()}")

        ret = {"connection_name": self.conn_name, "data": data}
        if autorequeue:
            asyncio.ensure_future(asyncio.create_task(self.receive_data(autorequeue=autorequeue)))
        if self.callback_function:
            self.callback_function(ret)
        else:
            return ret

    async def receive_file(self,autorequeue=False):
        meta = await self.conn.recv_string()
        meta_j = json.loads(meta)
        num_chunks = meta_j["num_chunks"]
        extension = meta_j["extension"]
        filename = f"/tmp/{getuuid()}{extension}"
        with open(filename, "wb") as out_file:
            for i in range(num_chunks):
                chunk = await self.conn.recv()
                out_file.write(chunk)

        await self.conn.send_string(json.dumps({"time": time.time()}))
        ret = {"connection_name": self.conn_name, "data": filename}
        if autorequeue:
            asyncio.ensure_future(asyncio.create_task(self.receive_data(autorequeue=autorequeue)))
        if self.callback_function:
            self.callback_function(ret)
        else:
            return ret




class SenderConnectionZMQ(connection_base):

    def __init__(self, host, port, datatype):
        super().__init__()
        self.host = host
        self.port = port
        self.datatype = datatype
        self.do_connection()

    def do_connection(self):
        if 'conn' in locals():
            self.conn.close()
        ctx = zmq.asyncio.Context()
        socket = ctx.socket(zmq.REQ)
        socket.connect(f"tcp://{self.host}:{self.port}")
        self.conn = socket
        #self.conn.setsockopt(zmq.LINGER, 5*1000)  # don't linger the connection indefinitely if the receiver endpoint does not exist. Only wait for 5 seconds and then error out
        logger.debug(f"Connecting to tcp://{self.host}:{self.port}")

        #socket.setsockopt(zmq.REQ_RELAXED, 1)  # relaxed socket connection
        #socket.setsockopt(zmq.REQ_CORRELATE, 1)  # correlate new socket connections

        if isinstance(self.datatype,str):
            try:
                from pydoc import locate
                self.datatype = locate(self.datatype)
                logger.debug(f"Located datatype: {self.datatype}")
            except:
                logger.debug(f"Could not locate datatype: {self.datatype}")


        if isinstance(self.datatype,(numpy.ndarray, numpy.generic)) or self.datatype == numpy.ndarray:
            logger.debug(f"Setting up numpy data send: {self.datatype}")
            self.send_data = self.sent_numpy_data
        elif isinstance(self.datatype,(str,int,float,list,dict,bool)) or any(self.datatype == c for c in (str,int,float,list,dict,bool)):
            logger.debug(f"Setting up native data send: {self.datatype}")
            self.send_data = self.send_native_data
        elif isinstance(self.datatype, FileTransfer) or self.datatype == FileTransfer:
            logger.debug(f"Setting up file send: {self.datatype}")
            self.send_data = self.send_file




    def closeConnection(self):
        self.conn.close()
        del self.conn

    async def sent_numpy_data(self,data):
        logger.debug("Sending numpy data")
        md = dict(
            dtype=str(data.dtype),
            shape=data.shape,
        )
        logger.debug("sending json metadata")
        await self.conn.send_string(json.dumps(md), 0 | zmq.SNDMORE)
        logger.debug("sending data")
        await self.conn.send(data, 0)
        logger.debug("awaiting reply")
        delay = await self.conn.recv()
        delay = json.loads(delay)
        logger.debug(f"Received reply: {delay}")
        return delay

    async def send_native_data(self, data):
        logger.debug("Sending native data")
        data = json.dumps(data)
        logger.debug(f"Sending data: {data}")
        await self.conn.send_string(data)
        logger.debug(f"Data sent")
        delay = await self.conn.recv()
        delay = json.loads(delay)

        return delay

    async def send_file(self,filename):
        logger.debug("Sending file")
        # we first read the file once to determine chunk numbers. Inefficient, but otherwise we have no number of chunks
        # TODO: Find out if we can somehow capture the "sendmore" flag from ZMQ and only rely on that
        chunk_num = 0
        chunk_size = 1024*1024 # 1MB chunk size
        with open(filename, "rb") as in_file:
            while True:
                chunk = in_file.read(chunk_size)
                if chunk == b"":
                    break
                chunk_num += 1

        # meta data
        meta = {"num_chunks":chunk_num,"filename":filename,"extension":os.path.splitext(filename)[1]}
        await self.conn.send_string(json.dumps(meta), 0 | zmq.SNDMORE)

        # send file data
        with open(filename, "rb") as in_file:
            for i in range(chunk_num):
                chunk = in_file.read(chunk_size)
                flag = 0 | zmq.SNDMORE if i < chunk_num-1 else 0
                await self.conn.send(chunk,flag)

        # receive ack with delay
        delay = await self.conn.recv()
        delay = json.loads(delay)
        return delay

async def test():
    log = logging.getLogger('')
    log.setLevel(logging.DEBUG)
    ch = logging.StreamHandler(sys.stdout)
    log.addHandler(ch)

    recv_conn = ReceiverConnectionZMQ("127.0.0.1", 5555, FileTransfer, "filetransfer")

    send_conn = SenderConnectionZMQ("127.0.0.1", 5555, FileTransfer)

    filename = "/home/murphy/Downloads/NodeGraphQt-main.zip"
    delay = asyncio.create_task(send_conn.send_file(filename))

    filename = await recv_conn.receive_file()

    print(f"Received file: {filename}")
    os.remove(filename["data"])

    delay = await delay
    print(delay)
    diff = time.time()-delay["time"]
    print(f"Delay: {diff}")

    send_conn = SenderConnectionZMQ("127.0.0.1", 5556, numpy.ndarray)
    recv_conn = ReceiverConnectionZMQ("0.0.0.0", 5556, numpy.ndarray, "numpy_transfer")

    data = numpy.random.rand(100,100)
    delay = asyncio.create_task(send_conn.send_data(data))
    data = await recv_conn.receive_data()
    print(f"Received data: {data}")

    send_conn = SenderConnectionZMQ("127.0.0.1", 5557, str)
    recv_conn = ReceiverConnectionZMQ("0.0.0.0", 5557, str, "str_transfer")

    se = "hello"
    delay = asyncio.create_task(send_conn.send_data(se))
    data = await recv_conn.receive_data()
    print(f"Received data: {data}")

if __name__ == "__main__":
    #run async test
    asyncio.run(test())