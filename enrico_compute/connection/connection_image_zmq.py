import time

import imagezmq
import zmq

from enrico_compute.connection.connection_base import connection_base


class connection_ImageZMQ(connection_base):

    def __init__(self, host, port):
        super().__init__()
        self.sender_conn = sender_ImageZMQ(host, port)
        self.receiver_conn = receiver_ImageZMQ(host, port)

    def register_callback(self, name, func):
        self.receiver_conn.register_callback(name, func)

    async def send_data(self,data):
        await self.sender_conn.send_data(data)

    async def receive_data(self):
        return self.receiver_conn.receive_data()

class sender_ImageZMQ(connection_base):

    def __init__(self, host, port):
        super().__init__()
        self.host = host
        self.port = port
        self.do_connection()

    def do_connection(self):
        if 'conn' in locals():
            self.conn.close()
        self.conn = imagezmq.ImageSender(connect_to=f'tcp://{self.host}:{self.port}')
        self.conn.zmq_socket.setsockopt(zmq.LINGER, 0) # kill the socket immediately
        self.conn.zmq_socket.setsockopt(zmq.RCVTIMEO, 5000)  # set a receive timeout
        self.conn.zmq_socket.setsockopt(zmq.SNDTIMEO, 5000)  # set a send timeout
        self.conn.zmq_socket.setsockopt(zmq.REQ_RELAXED, 1)  # relaxed socket connection
        self.conn.zmq_socket.setsockopt(zmq.REQ_CORRELATE, 1)  # correlate new socket connections

    async def send_data(self,data):
        try:
            await self.conn.send_image(time.time(), data)
        except (zmq.ZMQError, zmq.ContextTerminated, zmq.Again):
            print("Sender connection broken. Reconnecting...")
            self.do_connection()


class receiver_ImageZMQ(connection_base):

    def __init__(self, host, port):
        super().__init__()
        self.host = host
        self.port = port
        self.do_connection()
        self.callbacks = {}

    def do_connection(self):
        if 'conn' in locals():
            self.conn.close()
        self.conn = imagezmq.ImageHub(open_port=f'tcp://{self.host}:{self.port}')
        self.conn.zmq_socket.setsockopt(zmq.LINGER, 0) # kill the socket immediately
        self.conn.zmq_socket.setsockopt(zmq.RCVTIMEO, 5000)  # set a receive timeout
        self.conn.zmq_socket.setsockopt(zmq.SNDTIMEO, 5000)  # set a send timeout
        self.conn.zmq_socket.setsockopt(zmq.REQ_RELAXED, 1)  # relaxed socket connection
        self.conn.zmq_socket.setsockopt(zmq.REQ_CORRELATE, 1)  # correlate new socket connections

    def register_callback(self, name, func):
        self.callbacks[name] = func

    async def receive_data(self):
        try:
            in_time,data = await self.conn.recv_image()
            await self.conn.send_reply(time.time())
            for callback in self.callbacks.values():
                callback(data)
        except (zmq.ZMQError, zmq.ContextTerminated, zmq.Again):
            print("ZMQ Receiver connection broken. Reconnecting...")
            self.do_connection()
