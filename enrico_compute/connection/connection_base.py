from abc import abstractmethod
from threading import Thread


class connection_base():

    def __init__(self):
        super().__init__()
        #self.setDaemon(True)

    @abstractmethod
    async def send_data(self,data):
        raise NotImplementedError

    @abstractmethod
    async def receive_data(self,autorequeue=True):
        raise NotImplementedError

