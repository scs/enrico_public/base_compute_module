import asyncio
import logging
import os
import random
import sys
import time
import traceback
from abc import abstractmethod
from threading import Thread

import numpy as np
import psutil

from enrico_compute.configs.parameter_configs import NumberConfig, StringConfig, FileChooserConfig
from enrico_compute.connection.connection_ipaaca_information import Connection_Ipaaca, MessageObj, MessageStatus, MessageReason, ReceiverConnectionsInfo, ConnectionRequest, get_ip_address
from enrico_compute.connection.connection_logger import ConnLogHandler
from enrico_compute.connection.connection_zmq import ReceiverConnectionZMQ, SenderConnectionZMQ, FileTransfer

#os.environ['ENRICO_UUID'] = "0"
#os.environ['ENRICO_COMM_NAME'] = "main"

#os.environ['ENRICO_CONFIG_RUN'] = "true"
#os.environ["ENRICO_LIVE_RUN"] = "true"

logger = logging.getLogger(__name__)

#we define a custom exception to be raised if the sender has no more data to send
#Alternatively, we can just return an empty dictionary. Both works.
class SenderDoneException(Exception):
    pass



def check_socket_open(port):
    ports = psutil.net_connections()
    ports = set([p.laddr.port for p in ports])
    return port not in ports

def get_random_port():
    for _ in range(1000):
        port = 40000 + random.randrange(1, 20000)
        if check_socket_open(port):
            return port
    return None

class Meta(type):
    def __call__(cls, *args, **kwargs):
        logger.info("starting warmup")
        instance = super().__call__(*args, **kwargs)
        # config_run = not "ENRICO_LIVE_RUN" in os.environ.keys()
        # if config_run != "true":
        #     instance.load()
        #     ram,vram = instance.systemUsage()
        #     logger.info(f"System ressource Usage: {ram} bytes, {vram} bytes")
        #     logger.info("Warmup completed. Starting Module...")
        # else:
        #     logger.info("Just getting config. Not starting module")
        return instance


class BaseCompute(metaclass=Meta):

    def __init__(self, config):
        self.warmup_completed = False
        self.config = config
        self.conn_in_pipes = {}
        self.conn_out_pipes = {}
        self.open_receiver_info = ReceiverConnectionsInfo()
        self.getEnvironmentInfo()

        log = logging.getLogger('')
        log.setLevel(logging.INFO)
        self.conn_log = ConnLogHandler(self.uuid)
        Thread(target=self.conn_log.thread_sendMessages,daemon=True).start()

        log.addHandler(self.conn_log)
        sys.excepthook = self.error_handler


        self.input_variable_dict = {}
        self.output_variable_dict = {}

        self.can_visualize = True
        self.visualize_sender = None

        logger.info(f"I am {self.uuid}, with the communication name {self.comm_name}.")
        if self.config_run:
            logger.info(f"I am in config mode. I am performing a config run and will only test if the module is set up correctly.")
        elif self.live_run:
            logger.info("I am going live. I am running the module and will accept connections shortly.")
        else:
            logger.error("WHAT AM I?")


    def getEnvironmentInfo(self):
        self.uuid = os.environ['ENRICO_UUID'] if "ENRICO_UUID" in os.environ else "-1"
        self.comm_name = os.environ['ENRICO_COMM_NAME'] if 'ENRICO_COMM_NAME' in os.environ else "not_set"
        self.live_run = "ENRICO_LIVE_RUN" in os.environ.keys()
        self.config_run = not self.live_run




    @abstractmethod
    def load(self):
        raise NotImplementedError

    @abstractmethod
    def example_input(self):
        raise NotImplementedError

    @abstractmethod
    def compute(self, input_dict):
        raise NotImplementedError

    @abstractmethod
    def visualize(self,input_dict,output_dict):
        '''
        This method is used to visualize the data.
        It expects a numpy array in form of an image as return value, with the dimensions (width,height,3)
        It receives the last input dict and output dict as arguments

        :param input_dict: The last input dictionary with the keys listed in the config.module_inputs
        :param output_dict: The last output dictionary with the keys listed in the config.module_outputs

        return: numpy array with the dimensions (width,height,3)
        '''
        raise NotImplementedError

    def start(self):
        asyncio.run(self.run())

    def error_handler(self,exc_type, exc_value, exc_traceback):
        print(exc_type,exc_value, exc_traceback)
        logger.error("Uncaught exception: ", exc_info=(exc_type, exc_value, exc_traceback))
        if issubclass(exc_type, KeyboardInterrupt):
            sys.__excepthook__(exc_type, exc_value, exc_traceback)
            return
        else:
            msg = MessageObj()
            msg.uuid_from = self.uuid
            msg.modulename = self.config['module_name']
            msg.status = MessageStatus.ERROR
            msg.msg_reason = MessageReason.ERROR_INFO
            msg.msg_body = f"{exc_type} - {exc_value}"
            try:
                loop = asyncio.get_event_loop()
                future = loop.create_task(self.conn_info.sendMessage(msg))
                asyncio.ensure_future(future)
            except Exception:
                try:
                    loop = asyncio.new_event_loop()
                    loop.run_until_complete(self.conn_info.sendMessage(msg))
                except:
                    traceback.print_exc()



    async def get_connection_pipeline(self):
        input_pipes = self.config['module_inputs']
        if self.config_run:
            logging.info("Config run. Not creating connections.")
            return

        input_ports = []
        while len(input_ports) < len(input_pipes):
            port = 40000+random.randrange(1,20000)
            if port not in input_ports and check_socket_open(port):
                input_ports.append(port)

        for i,(k,v) in enumerate(input_pipes.items()):
            self.conn_in_pipes[k] = ReceiverConnectionZMQ("0.0.0.0", input_ports[i], v, k,callback_function=self.callback_compute)
            asyncio.ensure_future(asyncio.create_task(self.conn_in_pipes[k].receive_data(autorequeue=True)))

            #self.conn_in_pipes[k].setDaemon(True)
            #self.conn_in_pipes[k].start()

            (host,port,datatype,connection_name) = self.open_receiver_info.addOpenConnection(input_ports[i],v,k)
            logger.info(f"Receiver {k} started on port {input_ports[i]}")
            msg = MessageObj()
            msg.uuid_from = self.uuid
            msg.uuid_to = None
            msg.status = MessageStatus.OK
            msg.msg_reason = MessageReason.CONTAINER_STARTUP_INFO
            msg.connection_request = ConnectionRequest()
            msg.connection_request.ConnectionName = k
            msg.connection_request.Uuid = self.uuid
            msg.connection_request.Host = host
            msg.connection_request.Port = port
            await self.conn_info.sendMessage(msg)

        logging.info("Trying if visualizer is implemented")
        #check if we can visualize and if yes, send msg. We rely on the startup test to error check the actual visualize function.
        try:
            self.visualize([],[])
        except NotImplementedError:
            # visualize is not implemented.
            self.can_visualize = False
            logging.info("Visualizer function is not implemented.")
        except Exception:
            pass
            # visualize exceptions are allowed if we give empty dictionaries. This is actually alright (even if an example of bad coding)
            # TODO: Maybe we should run visualize twice (with arguments and without) to force some defensive coding in the module?

        if self.can_visualize:
            logging.info("Sending Visualizer info")
            # we send an empty visualize request to notify that we could send a visualize image if requested.
            msg = MessageObj()
            msg.uuid_from = self.uuid
            msg.uuid_to = None
            msg.status = MessageStatus.OK
            msg.msg_reason = MessageReason.VISUALIZE_REQUEST
            msg.connection_request = None
            await self.conn_info.sendMessage(msg)

    def handle_connections(self,msg_obj: MessageObj):
        #logging.info("got message")
        #ignore our own messages
        if msg_obj.uuid_from == self.uuid:
            return

        logging.debug(f"I am {self.uuid} - Got message from {msg_obj.uuid_from} to {msg_obj.uuid_to} with reason {msg_obj.msg_reason}")


        if msg_obj.uuid_to is None: #broadcast or specific requests only

            #check if we are connected to the container that is shutting down
            if msg_obj.msg_reason == MessageReason.CONTAINER_SHUTDOWN_INFO:
                uuid_proc = msg_obj.connection_request.Uuid
                for pipe_name,conn_list in self.conn_out_pipes.items():
                    if uuid_proc in conn_list.keys():
                        for conn in conn_list[uuid_proc]:
                            conn.closeConnection()
                        del conn_list[uuid_proc]

                        # send info to inform of updated connections. TODO: Do we currently track connections in the ModuleInfo?
                        loop = asyncio.get_event_loop()
                        task = loop.create_task(self.sendModuleInfo())
                        asyncio.ensure_future(task)


            return #Do we need to know the info of other modules?

        if msg_obj.uuid_to == self.uuid: # dis/connect request
            #logging.debug("Message adressed to me.")
            if msg_obj.msg_reason == MessageReason.CONNECT_REQUEST or msg_obj.msg_reason == MessageReason.DISCONNECT_REQUEST:
                if msg_obj.connection_request is None:
                    logger.error("No connection request in message")
                    return

                connect_host = msg_obj.connection_request.Host              # the connection host
                connect_port = msg_obj.connection_request.Port              # the connection port
                connect_Name = msg_obj.connection_request.ConnectionName    # our outbounding connection that should be used to send to the other connection.
                connect_uuid = msg_obj.connection_request.Uuid              # the uuid of the other process.

                if msg_obj.msg_reason == MessageReason.CONNECT_REQUEST:
                    logger.info(f"Received connection request from {connect_uuid} on {connect_host}:{connect_port}")
                    if connect_Name in self.config['module_outputs'].keys():
                        temp_dict = {}
                        if connect_Name in self.conn_out_pipes:
                            temp_dict = self.conn_out_pipes[connect_Name]
                        if connect_uuid in temp_dict:
                            logger.error(f"ONLY ONE CONNECTION ALLOWED! TODO: ERROR HANDLING")

                        logger.info(f"Connecting to {connect_uuid} on {connect_host}:{connect_port} with datatype {self.config['module_outputs'][connect_Name]}")

                        temp_dict[connect_uuid] = SenderConnectionZMQ(connect_host, connect_port, self.config['module_outputs'][connect_Name]) #Only one connection to one Receiver of the same uuid allowed.
                        self.conn_out_pipes[connect_Name] = temp_dict #manually setting, because of paranoia
                    else:
                        logging.info(f"Connection request from {connect_uuid} on {connect_host}:{connect_port} for {connect_Name} not in module outputs. Ignoring.")


                if msg_obj.msg_reason == MessageReason.DISCONNECT_REQUEST:
                    if connect_Name in self.config['module_outputs'].keys() and connect_Name in self.conn_out_pipes and connect_uuid in self.conn_out_pipes[connect_Name]:
                        sender = self.conn_out_pipes[connect_Name].pop(connect_uuid)
                        sender.close()
                        logger.info(f"Disconnecting from {connect_uuid} on {connect_host}:{connect_port}")


            if msg_obj.msg_reason == MessageReason.CONTAINER_STOP_REQUEST:
                loop = asyncio.get_event_loop()
                task = loop.create_task(self.shutdown())
                asyncio.ensure_future(task)

            if msg_obj.msg_reason == MessageReason.VISUALIZE_REQUEST:
                if self.can_visualize:
                    logging.info("We got a visualize request. Sending visualize image.")
                    asyncio.ensure_future(asyncio.create_task(self.sendvisualize(msg_obj.connection_request)))
                else:
                    logging.info("We got a visualize request, but we cannot visualize. Ignoring.")

        else:
            logger.info(f"I got a weird message and will not adhere to the commands {msg_obj.msg_reason} from {msg_obj.uuid_from} to {msg_obj.uuid_to}")


    async def sendvisualize(self,conn_request : ConnectionRequest):


        #create sender if not already done
        if self.visualize_sender is None:
            logging.info("Creating Visualize Sender Connection")
            self.visualize_sender = SenderConnectionZMQ(conn_request.Host, conn_request.Port, np.ndarray)

        #need last input and output if there is input/output. Otherwise, we don't send any data.
        if (len(self.input_variable_dict.keys()) > 0 or self.config['module_inputs'].keys() == 0) or (len(self.output_variable_dict.keys()) > 0 or self.config['module_outputs'].keys() == 0):
            logging.info("sending a visualize image.")
            in_dict = {k:v[-1] for k,v in self.input_variable_dict.items()}
            out_dict = {k:v[-1] for k,v in self.output_variable_dict.items()}
            try:
                img = self.visualize(in_dict,out_dict)
                if img is not None:
                    await self.visualize_sender.send_data(img)
            except Exception as e:
                logging.exception(e)
        else:
            logging.info("The module did not run yet, so we can't send a visualize image.")



    def callback_compute(self,ret_dict):
        logging.debug(f"COMPUTE: calling compute")
        # get the result of the task
        # get the connection name and the data
        conn_name = ret_dict["connection_name"]
        value = ret_dict["data"]

        # add to the input variable history
        re = []
        if conn_name in self.input_variable_dict:
            re = self.input_variable_dict[conn_name]
        re.append(value)
        re = re[-self.config['history_length']:]
        self.input_variable_dict[conn_name] = re

        # create a dictionary with the connection name and the data for the compute function
        compute_dict = {}
        for k,v in self.input_variable_dict.items():
            compute_dict[k] = v[-1]
            compute_dict[k + "_history"] = v

        # add None to the compute_dict if the connection name is not in the compute_dict (this should only be at the start, or if one connection is not established)
        for k in self.config['module_inputs'].keys():
            if k not in compute_dict:
                compute_dict[k] = None
                compute_dict[k + "_history"] = None

        logging.debug(f"COMPUTE: calling compute")
        # call the compute function
        try:
            ret_dict = self.compute(compute_dict)
        except Exception as e:
            traceback.print_exc()
            logging.exception(e)
        logging.debug(f"COMPUTE: sending data")
        # send the data to all outgoing connections
        for k, v in ret_dict.items():
            #set output for visualization and history
            re = self.output_variable_dict.get(k,[])
            re.append(v)
            re = re[-self.config['history_length']:]
            self.output_variable_dict[k] = re

            if k in self.conn_out_pipes:
                for sender in self.conn_out_pipes[k].values():
                    logging.info(f"Sending data to {k}")
                    loop = asyncio.get_event_loop()
                    task = loop.create_task(sender.send_data(v))
                    asyncio.ensure_future(task)
            else:
                logging.debug(f"sender key {k} not in output connections. Ignoring.")
        logging.debug(f"COMPUTE: sent data")

    async def get_config_data(self):
        async def sendDataRequest(k,port_in):
            msg = MessageObj()
            msg.msg_reason = MessageReason.CONFIG_DATA_REQUEST
            msg.status = MessageStatus.STARTUP
            msg.uuid_from = self.uuid
            msg.uuid_to = None
            msg.connection_request = ConnectionRequest(Host=get_ip_address(),Port=port_in)
            msg.msg_body = k
            await self.conn_info.sendMessage(msg)

        print(self.config)
        gui_configs = {k: v for k, v in self.config.items() if isinstance(v, (NumberConfig, StringConfig, FileChooserConfig))}
        print(f"found {len(gui_configs)} gui elements that need config infos")
        for k,v in gui_configs.items():
            logger.info(f"Getting config data for {k}")
            port = get_random_port()
            # we need to receive the file and adjust the path to the file path if it is a filechooserconfig else we just get the data
            datatype = FileTransfer if isinstance(v, FileChooserConfig) else str # defaults to native type if not file (This means we receive json, which can have different types)
            receive = ReceiverConnectionZMQ("0.0.0.0", port, datatype, k, callback_function=None)
            await sendDataRequest(k,port) # we request the config data
            ret_dict = await receive.receive_data()
            data = ret_dict["data"]
            v.set_value(data) # In all cases we just set the config data. We assume that the modules know what they want and what they need.

            #clean up connection after receiving the data
            #currently does not work and throws exceptions TODO: fix
            #receive.conn.close()
            del receive



    async def run(self):

        self.conn_info = Connection_Ipaaca(self.handle_connections, communication_host="radioactiveman", communication_port="1883", communication_identifier=self.comm_name)
        await self.conn_info.create_communication_connection()
        self.conn_log.setConnection(self.conn_info)

        #during development, we only test if the module runs correctly
        if not self.live_run:
            logger.info("DEVELOPMENT MODE: running module once to check if module is working")
            self.load()
            ret = self.compute(self.example_input())
            try:
                self.visualize(self.example_input(),ret)
            except NotImplementedError:
                # ignore notimplementederror for container without visualize method.
                # all other errors will still be thrown, to allow debug
                pass


            logger.info("DEVELOPMENT MODE: got the following return:")
            logger.info(ret)
            await self.sendModuleInfo()
            await asyncio.sleep(1) # sending other logger infos
            sys.exit(0)
        else:
            logger.info("LIVE MODE: running module")
            # check what configs are defined and wait for the config to be sent.
            # we deliberately await this function to ensure that the module is set up correctly
            await self.get_config_data()
            self.load()

        logger.info("Starting module. Establishing connections.")

        #create message pipeline
        await self.get_connection_pipeline()

        logger.info("Connections established.")

        #create periodic message sending task
        periodic_task = asyncio.create_task(self.periodicInfo())
        asyncio.ensure_future(periodic_task)

        sender_override_started = False

        #check if we have a sender only module and add a coroutine if this is the case
        if 'sender_override' in self.config and self.config['sender_override']:
            logging.info("Creating Sender only worker")
            periodic_task = asyncio.create_task(self.sender_override())
            asyncio.ensure_future(periodic_task)
            sender_override_started = True

        #add all input pipelines to the running tasks
        self.running_tasks = {}


        #logger.info(f"Starting receiver tasks: {self.conn_in_pipes}")
        #for k,v in self.conn_in_pipes.items():
        #    task = asyncio.create_task(self.run_receiver(k,v))
        #    asyncio.ensure_future(task)

        msg = MessageObj()
        msg.uuid_from = self.uuid
        msg.uuid_to = None
        msg.status = MessageStatus.OK
        msg.msg_reason = MessageReason.ACKNOWLEDGE
        msg.msg_ack_reason = MessageReason.CONTAINER_START_REQUEST
        await self.conn_info.sendMessage(msg)

        #only do if there are input connections
        if self.conn_in_pipes.keys() == 0:
            logger.info("We are a sender only container, so we wait for connections and will then send our data")
            if not sender_override_started:
                logger.warning("We are a sender only container, but have no sender_override coroutine. This will lead to a deadlock. Automatically starting sender_override coroutine with default interval of 1s")
                self.config['sender_override'] = True
                self.config['sender_override_interval'] = 1
                periodic_task = asyncio.create_task(self.sender_override())
                asyncio.ensure_future(periodic_task)
            #if there are no input connections, just wait endless for the periodic info/sender task
        while True:
            await asyncio.sleep(1)


    async def sender_override(self):
        """
            This is an asynchronous method that continuously sends data to all established connections.
            It waits until all connections are established before sending data.
            The data is computed by calling the compute method with an empty dictionary as input.
            The computed data is then sent to all connections.
            If the compute method does not return any data, the function breaks from the loop.
            If a SenderDoneException is caught, the function also breaks from the loop.
            After sending data, the function sleeps for a certain amount of time, ensuring that the sleep time adheres to the 'sender_override_interval' specified in the configuration.
            When the sender is done, it sends a shutdown message and exits the program.
        """
        logging.debug("Sender process started")
        while True:
            # we wait until all connections are established. Otherwise, we might send to a non-existing connection and omit the first data packages.
            while len(self.conn_out_pipes.keys()) < len(self.config["module_outputs"].keys()):
                logging.info(f"Waiting for all connections to be established. Currently established: {len(self.conn_out_pipes.keys())} of {len(self.config['module_outputs'].keys())}")
                await asyncio.sleep(5)

            # compute data
            try:
                last_exec_time = time.time()
                ret_dict = self.compute({})
                logging.debug(f"Sending compute data: {ret_dict}")
                # send data to all connections


                for k,v in ret_dict.items():
                    # set output for visualization and history
                    re = self.output_variable_dict.get(k, [])
                    re.append(v)
                    re = re[-self.config['history_length']:]
                    self.output_variable_dict[k] = re

                    if k in self.conn_out_pipes.keys():
                        logger.debug(f"Step 2 - Sending data to {k}")
                        if len(self.conn_out_pipes[k].values()) == 0:
                            logging.debug(f"sender key {k} has no connections. Ignoring.")
                        for sender in self.conn_out_pipes[k].values():
                            logging.info(f"Sending data to {k}")
                            await sender.send_data(v)
                    else:
                        logging.warning(f"sender key {k} not in output connections. Ignoring.")

            except SenderDoneException:
                break
            except Exception as e:
                traceback.print_exc()
                logging.exception(e)

            # we ensure that we adhere to the sleep time, by removing the compute delay from the sleep time.
            corrected_wait_time = 1/self.config['sender_override_interval']
            corrected_wait_time = max(0.00000001,corrected_wait_time-(time.time()-last_exec_time))

            await asyncio.sleep(corrected_wait_time)

        # Sender is done
        logger.info("Sender is done. Exiting")
        msg = MessageObj()
        msg.uuid_from = self.uuid
        msg.modulename = self.config['module_name']
        msg.status = MessageStatus.SHUTDOWN
        msg.msg_reason = MessageReason.SENDER_DONE
        await self.conn_info.sendMessage(msg)
        await self.shutdown()

    async def shutdown(self):
        """
            This is an asynchronous method that sends a shutdown message to all established connections.
            The method sends a shutdown message to all established connections by calling the sendShutdown method.
            The method then waits for all connections to be closed before returning.
        """
        msg = MessageObj()
        msg.uuid_from = self.uuid
        msg.modulename = self.config['module_name']
        msg.status = MessageStatus.SHUTDOWN
        msg.msg_reason = MessageReason.CONTAINER_SHUTDOWN_INFO
        await self.conn_info.sendMessage(msg)
        logging.info(f"Container {self.config['module_name']} with uuid {self.uuid} is shutting down.")
        await asyncio.sleep(1)
        sys.exit(0)


    async def periodicInfo(self):
        """
            This is an asynchronous method that periodically sends module information.
            It sends the module information every 30 seconds by calling the sendModuleInfo method.
            The method runs indefinitely in a while loop.
        """
        while True:
            await self.sendModuleInfo()
            await asyncio.sleep(30)


    async def sendModuleInfo(self):
        msg = MessageObj()
        msg.uuid_from = self.uuid
        msg.modulename = self.config['module_name']
        msg.status = MessageStatus.OK
        msg.msg_reason = MessageReason.PING_INFO
        msg.open_receiver_connections = self.open_receiver_info.getConnections()
        msg.module_config = self.config
        ram,vram = self.systemUsage()
        msg.ram_usage = ram
        msg.vram_usage = vram
        logger.info("sending module info config")
        await self.conn_info.sendMessage(msg)


    def systemUsage(self):
        self.ram = psutil.Process(os.getpid()).memory_info().rss
        try:
            import torch
            self.gpu_ram = torch.cuda.memory_allocated()
            logger.info(f"Memory usage: {self.ram} bytes, GPU memory usage: {self.gpu_ram} bytes")
            return self.ram, self.gpu_ram
        except:
            logger.info(f"Memory usage: {self.ram} bytes, GPU memory usage: Not available")
            return self.ram, -1



