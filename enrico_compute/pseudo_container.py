import inspect



#think about categorizing objects!

def StringOperator_concat(a : str,b : str) -> str:
    '''
    Concatenate two strings
    '''
    return a+b

def NumberOperator_toFloat(a : int) -> float:
    '''
    Convert an Integer to Float
    '''
    return float(a)


def parse_pseudo_container():
    from typing import get_type_hints
    me = __import__(inspect.getmodulename(__file__))
    functions = [m for m in dir(me) if inspect.isfunction(getattr(me, m)) and m != "parse_pseudo_container"]
    for func_name in functions:
        func = getattr(me, func_name)
        hints = get_type_hints(func)
        print(func,hints)
        print(func.__doc__.strip())
        #print(help(func))


if __name__ == "__main__":
    parse_pseudo_container()